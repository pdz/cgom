"""
ST: 'cGOM'
Bachmann David, Hess Stephan & Julian Wolf (SV)
pdz, ETH Zürich
2018

This file contains all functions to perform gaze inference.
"""

# Global imports
import os
import sys
import skimage
import warnings
import argparse
import cv2
import numpy as np

# Local imports
import utils

# Import Mask RCNN from parent directory
sys.path.append('..')
from mrcnn.config import Config
from mrcnn import model as modellib

# Suppress warnings
warnings.filterwarnings('ignore', message='Anti-aliasing will be enabled by default in skimage 0.15 to')

# Import default_configs
parser = argparse.ArgumentParser(description='Map the gaze coordinates to object names.')
parser.add_argument('--config', default='./default_configs/make_gaze.yaml')
c = parser.parse_args()
args = utils.read_config(c.config)

# Derived config class
class GazeConfig(Config):

    # Name
    NAME = "gaze"

    # Number of GPUs
    GPU_COUNT = 1

    # Number of images per GPU
    IMAGES_PER_GPU = 1

    # Detection confidence
    DETECTION_MIN_CONFIDENCE = args['detection_min_confidence']

def track_gaze(model, classes, name):

    # Directories
    VIDEO_PATH = os.path.join(args['video_dir'], name + '.avi')
    GAZE_PATH = os.path.join(args['gaze_dir'], name + '.txt')
    IMAGE_DIR = os.path.join(args['image_output_dir'], name)
    FILE_DIR = args['file_output_dir']
    utils.make_path(IMAGE_DIR)
    utils.make_path(FILE_DIR)

    # Video capture
    video = cv2.VideoCapture(VIDEO_PATH)
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = video.get(cv2.CAP_PROP_FPS)

    # Read the gaze file and make it iterable
    gaze = utils.read_gaze(GAZE_PATH, max_res=(height, width))
    gaze = iter(gaze)

    # Set up an info buffer
    info_buffer = []

    # Get the first entry from the gaze file
    gaze_entry = next(gaze)
    (t_start, t_end, x, y) = list(gaze_entry.values())

    # If num_f is selected to be zero we choose the central frame from the fixation, else random frames are taken
    f_start = int(t_start * fps)
    f_end = int(t_end * fps)
    if args['num_f'] == 0:
        f_rand = [int((f_end + f_start + 2) / 2)]
    # Get random frames from the fixation interval
    # Shift the range, since np.arange takes a lower half open interval
    # We require this since int() crops the values and we use the frame count instead of the ID
    # Like this the lowest "frame ID" is 1, however, the lowest frame would be zero, since eg. >int(0.7) 0.
    else:
        f_range = np.arange(f_start, f_end) + 1
        rand_size = np.minimum(f_range.size, args['num_f'])
        # It might happen that both time steps lie within the same frame, hence the array would be of size 0
        # If that is the case, use f_start
        if f_range.size > 0:
            f_rand = np.random.choice(f_range, rand_size, replace=False)
        else:
            f_rand = [f_start + 1]

    # Go through the frames of the video
    f_count = 0
    video_flag = True
    score_tracker = np.zeros(len(classes))
    while video_flag:

        # Read a frame
        video_flag, frame = video.read()
        f_count += 1

        # Just to be sure
        assert f_count <= max(f_rand), 'For some reason the frame counter surpassed all values in the random array.'

        # If a frame is within our random array we keep it
        if f_count in f_rand:
            assert video_flag, 'A time outside the scope of the video has been selected. This should not happen.'

            # Perform detection on the frame
            # Don't forget to flip it, due to open cv
            frame = np.flip(frame, axis=2).copy()
            r = model.detect([frame], verbose=0)[0]

            # Check all masks, keep track of the scores
            # Only keep the top scoring mask
            masks = r['masks'].astype('uint8')
            scores = r['scores']
            class_ids = r['class_ids']

            # Diliate the masks if requested
            kernel = np.ones((args['dilation'], args['dilation']), dtype='uint8')
            dil_masks = np.zeros_like(masks)
            for i in range(masks.shape[-1]):
                dil_masks[:, :, i] = cv2.dilate(masks[:, :, i], kernel, iterations=1)
            masks = dil_masks

            # Check where the gaze point lies
            num_masks = masks.shape[-1]
            max_score = 0.
            max_class = 0
            for i in range(num_masks):
                if masks[y, x, i] == True and scores[i] > max_score:
                    max_score = scores[i]
                    max_class = class_ids[i]

            # The top scoring label get a vote with a weight corresponding to its score
            # If no mask was detected BG is assumed with a specified score
            # It would make sense to use 1 as BG score, however, this is a bit strong
            if max_score > 0.:
                assert max_class != 0, 'max_class is equal to 0. This should not happen, since class_ids does not contain BG'
                score_tracker[max_class] += max_score
            else:
                score_tracker[0] += args['BG_score']

            # If the fixation is exhausted, generate the required outputs and get new frame IDs
            # This is the case if the counter has reached the largest frame ID in the random array
            if f_count == max(f_rand):

                # Find the argmax from the score_tracker
                # Note that we don't need to average
                label = classes[np.argmax(score_tracker)]

                # Save the info for later
                info_buffer.append({'start_time': t_start, 'end_time': t_end, 'label':label})

                # Write the latest frame to an image if you please
                if args['image_output_dir'] != None:
                    cv2.circle(frame, (x, y), int(frame.shape[0]/100.), (255, 0, 0), thickness=int(frame.shape[0]/100.))
                    text = 'start_time: %f end_time: %f label: %s' %(t_start, t_end, label)
                    cv2.putText(frame, text, (int(frame.shape[0]/50.), int(frame.shape[0]/50.)), cv2.FONT_HERSHEY_PLAIN, frame.shape[0]/700., (255, 0, 0))
                    skimage.io.imsave(os.path.join(IMAGE_DIR, str(f_count) + '.JPG'), frame)

                # Select a new entry from the gaze file, if it is exhausted break the loop
                gaze_entry = next(gaze, 'break')
                if gaze_entry == 'break':
                    break
                (t_start, t_end, x, y) = list(gaze_entry.values())

                # Either get the central frame or random frames
                f_start = int(t_start * fps)
                f_end = int(t_end * fps)
                if args['num_f'] == 0:
                    f_rand = [int((f_end + f_start + 2) / 2)]
                else:
                    # Get random frames from the fixation interval
                    f_range = np.arange(f_start, f_end) + 1
                    rand_size = np.minimum(f_range.size, args['num_f'])
                    if f_range.size > 0:
                        f_rand = np.random.choice(f_range, rand_size, replace=False)
                    else:
                        f_rand = [f_start + 1]

                # Reset the score tracker array
                score_tracker = np.zeros(len(classes))

    video.release()

    # Write the info buffer to a file
    utils.write_gaze(info_buffer, os.path.join(FILE_DIR, name + '.txt'))


if __name__ == '__main__':

    # Read the labels
    LABEL_DIR = os.path.join(args['label_dir'], 'labels.json')
    classes = list(utils.load(LABEL_DIR).keys())

    # Configs for model
    class GazeConfig(GazeConfig):
        NUM_CLASSES = len(classes)
    config = GazeConfig()
    config.display()

    # Load the model
    model = modellib.MaskRCNN(mode="inference", config=config, model_dir=args['log_dir'])

    # Load weights
    if args['weights_path'] == 'last':
        model.load_weights(model.find_last(), by_name=True)
    else:
        model.load_weights(args['weights_path'], by_name=True)

    # Iterate through all videos and gaze files
    gaze_names = [name.split('.')[0] for name in os.listdir(args['gaze_dir'])]
    for video_name in os.listdir(args['video_dir']):
        # Detach name from suffix
        name = video_name.split('.')[0]
        # Check if corresponding gazefile exists
        assert name in gaze_names, "Corresponding gaze file does not exists."
        # Run the tracker function
        track_gaze(model, classes, name)
